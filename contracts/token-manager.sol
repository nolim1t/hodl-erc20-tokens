pragma solidity ^0.4.23;

library SafeMath {
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}

contract Ownable {
    address public owner;

    /**
      * @dev The Ownable constructor sets the original `owner` of the contract to the sender
      * account.
      */
    function Ownable() public {
        owner = msg.sender;
    }

    /**
      * @dev Throws if called by any account other than the owner.
      */
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    /**
    * @dev Allows the current owner to transfer control of the contract to a newOwner.
    * @param newOwner The address to transfer ownership to.
    */
    function transferOwnership(address newOwner) public onlyOwner {
        if (newOwner != address(0)) {
            owner = newOwner;
        }
    }

}

contract Pausable is Ownable {
  event Pause();
  event Unpause();

  bool public paused = false;


  /**
   * @dev Modifier to make a function callable only when the contract is not paused.
   */
  modifier whenNotPaused() {
    require(!paused);
    _;
  }

  /**
   * @dev Modifier to make a function callable only when the contract is paused.
   */
  modifier whenPaused() {
    require(paused);
    _;
  }

  /**
   * @dev called by the owner to pause, triggers stopped state
   */
  function pause() onlyOwner whenNotPaused public {
    paused = true;
    Pause();
  }

  /**
   * @dev called by the owner to unpause, returns to normal state
   */
  function unpause() onlyOwner whenPaused public {
    paused = false;
    Unpause();
  }
}

/**
 * @title ERC20Basic
 * @dev Simpler version of ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/20
 */
contract ERC20Basic {
    uint public _totalSupply;
    function totalSupply() public constant returns (uint);
    function balanceOf(address who) public constant returns (uint);
    function transfer(address to, uint value) public;
    event Transfer(address indexed from, address indexed to, uint value);
}

/**
 * @title ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/20
 */
contract ERC20 is ERC20Basic {
    function allowance(address owner, address spender) public constant returns (uint);
    function transferFrom(address from, address to, uint value) public;
    function approve(address spender, uint value) public;
    event Approval(address indexed owner, address indexed spender, uint value);
}


// The actual contract
contract TokenManager is Ownable, Pausable {
  struct Transfer {
    address contract_;
    address to_;
    uint amount_;
    bool failed_;
  }

  mapping(address => uint[]) public transactionIndexesToSender;
  mapping(bytes32 => address) public tokens;

  Transfer[] public transactions;
  ERC20 public ERC20Interface;

  event TransferSuccessful(address indexed from_, address indexed to_, uint256 amount_);
  event TransferFailed(address indexed from_, address indexed to_, uint256 amount_);

  function addNewToken(bytes32 symbol_, address address_) public onlyOwner returns (bool) {
    tokens[symbol_] = address_;
    return true;
  }
  function removeToken(bytes32 symbol_) public onlyOwner returns (bool) {
    require(tokens[symbol_] != 0x0);
    delete(tokens[symbol_]);
    return true;
  }

  // Transfer token stuff
  function transferTokens(bytes32 symbol_, address to_, uint256 amount_) public whenNotPaused{
    require(tokens[symbol_] != 0x0);
    require(amount_ > 0);
    address contract_ = tokens[symbol_];
    address from_ = msg.sender;
    ERC20Interface = ERC20(contract_);

    uint256 transactionId = transactions.push(
      Transfer({
        contract_:  contract_,
        to_: to_,
        amount_: amount_,
        failed_: true
      })
    );
    transactionIndexesToSender[from_].push(transactionId - 1);

    // Revert if amount is more than allowance
    if(amount_ > ERC20Interface.allowance(from_, address(this))) {
      emit TransferFailed(from_, to_, amount_);
      revert();
    }
    // Otherwise transfer and emit Transfer Successful event
    ERC20Interface.transferFrom(from_, to_, amount_);
    transactions[transactionId - 1].failed_ = false;
    emit TransferSuccessful(from_, to_, amount_);
  }

  // Allow to receive stuff
  function() public payable {}

  function withdraw(address beneficiary) public payable onlyOwner whenNotPaused {
    beneficiary.transfer(address(this).balance);
  }
  
}
